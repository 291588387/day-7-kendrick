## O:

- Drew concept map to review on what we've learn. Strengthened comprehension of TDD
- Introduced HTTP basic, including URL, HTTP methods, HTTP response codes.
- Learned Restful API, it use HTTP verbs to let server process resources.
- Introduced Pair Programming, with its benefits and specification.
- Studied usage of Spring Boot framework, knows how to develop controller layer with framework.

## R:

- Focused and enlightened

## I:

- The introduction and benefits of pair programing have broaden my horizon and enlightened me a lot about how to promote efficiency in pair programming
- Restful api and springboot tutorial is so important that they kept me focus.

## D:

- Explore more on Restful API, pursue cleanness and readability in developing controller layer code.
- Know more spring boot annotations and make good use of them.