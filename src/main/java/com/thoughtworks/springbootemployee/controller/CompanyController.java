package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @GetMapping
    public List<Company> getAllCompanies() {
        return companyRepository.getCompanies();
    }

    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable Long id) {
        return companyRepository.findById(id);
    }

    @PostMapping
    public Company createCompany(@RequestBody Company company) {
        return companyRepository.save(company);
    }

    @PutMapping("/{id}")
    public Company updateCompanyById(@PathVariable Long id, @RequestBody Company company) {
        return companyRepository.updateById(id, company);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompanyById(@PathVariable Long id) {
        companyRepository.deleteById(id);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> pageQuery(@RequestParam int page, @RequestParam int size) {
        return companyRepository.pageQuery(page, size);
    }

    @GetMapping("/{id}/employees")
    public List<Employee> getEmployeesByCompanyId (@PathVariable Long id) {
        return employeeRepository.findByCompanyId(id);
    }

}
