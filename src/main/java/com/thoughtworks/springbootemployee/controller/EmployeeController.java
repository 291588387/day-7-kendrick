package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/employees")
public class EmployeeController {
    @Autowired
    EmployeeRepository employeeRepository;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Employee createEmployee(@RequestBody Employee employee) {
        return employeeRepository.save(employee);
    }

    @GetMapping
    public List<Employee> getEmployeeList() {
        return this.employeeRepository.getEmployees();
    }

    @GetMapping("/{id}")
    public Employee getEmployeeById(@PathVariable Long id) {
        return employeeRepository.findById(id);
    }

    @GetMapping(params = {"gender"})
    public List<Employee> filterEmployeeByGender(@RequestParam String gender) {
        return employeeRepository.findByGender(gender);
    }

    @PutMapping("{id}")
    public Employee updateEmployeeById(@PathVariable Long id, @RequestBody Employee inputEmployee) {
        return employeeRepository.updateById(id, inputEmployee);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable Long id) {
        employeeRepository.deleteById(id);
    }

    @GetMapping(params = {"page", "size"})
    public List<Employee> pageQuery(@RequestParam int page, @RequestParam int size) {
        return employeeRepository.pageQuery(page, size);
    }

}
