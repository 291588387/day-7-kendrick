package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {
    private final List<Company> companies = new ArrayList<>();
    @Autowired
    private EmployeeRepository employeeRepository;

    public Company save(Company company) {
        company.setId(generateId());
        companies.add(company);
        return company;
    }

    public void delete(Company company) {
        employeeRepository.getEmployees().stream()
                .filter(employee -> employee.getCompanyId().equals(company.getId()))
                .forEach(employee -> employee.setCompanyId(null));
        companies.remove(company);
    }

    public Company findById(Long id) {
        return companies.stream()
                .filter(company -> company.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public List<Company> getCompanies() {
        return companies;
    }


    public List<Company> pageQuery(int page, int size) {
        if (page <= 0 || size <= 0) return new ArrayList<>();
        return companies.stream()
                .skip((long) (page - 1) *size)
                .limit(size)
                .collect(Collectors.toList());
    }

    private Long generateId() {
        return companies.stream()
                .max(Comparator.comparingLong(Company::getId))
                .map(employee -> employee.getId() + 1)
                .orElse(1L);
    }

    public Company updateById(Long id, Company company) {
        Company fetchedCompany = companies.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElse(null);
        if (!Objects.isNull(fetchedCompany)) {
            fetchedCompany.setName(company.getName());
        }
        return fetchedCompany;
    }

    public void deleteById(Long id) {
        Company fetchedCompany = companies.stream()
                .filter(company -> company.getId().equals(id))
                .findFirst()
                .orElse(null);
        companies.remove(fetchedCompany);
    }
}
