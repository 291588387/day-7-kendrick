package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {
    private final List<Employee> employees = new ArrayList<>();

    public Employee save(Employee employee) {
        employee.setId(generateId());
        employees.add(employee);
        return employee;
    }

    public void deleteById(Long id) {
        Employee fetchedEmployee = employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public Employee findById(Long id) {
        return employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public List<Employee> findByGender(String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equalsIgnoreCase(gender))
                .collect(Collectors.toList());
    }

    private Long generateId() {
        return employees.stream()
                .max(Comparator.comparingLong(Employee::getId))
                .map(employee -> employee.getId() + 1)
                .orElse(1L);
    }

    public Employee updateById(Long id, Employee inputEmployee) {
        Employee fetchedEmployee = employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElse(null);
        if (!Objects.isNull(fetchedEmployee)) {
            fetchedEmployee.setAge(Objects.isNull(inputEmployee.getAge()) ? inputEmployee.getAge() : fetchedEmployee.getAge());
            fetchedEmployee.setSalary(Objects.isNull(inputEmployee.getSalary()) ? inputEmployee.getSalary() : fetchedEmployee.getSalary());
            fetchedEmployee.setCompanyId(Objects.isNull(inputEmployee.getCompanyId()) ? inputEmployee.getCompanyId() : fetchedEmployee.getCompanyId());
        }
        return fetchedEmployee;
    }

    public List<Employee> findByCompanyId(Long id) {
        return employees.stream()
                .filter(employee -> id.equals(employee.getCompanyId()))
                .collect(Collectors.toList());
    }


    public List<Employee> pageQuery(int page, int size) {
        if (page <= 0 || size <= 0) return new ArrayList<>();
        return employees.stream()
                .skip((long) (page - 1) *size)
                .limit(size)
                .collect(Collectors.toList());
    }
}
